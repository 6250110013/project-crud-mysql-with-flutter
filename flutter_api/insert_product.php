<?php
 
/*
 * Following code will create a new product row
 * All product details are read from HTTP Post Request
 */
 
// array for JSON response
$response = array();

//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");	 

// check for required fields
if (!empty($_POST['name']) and !empty($_POST['price']) and !empty($_POST['description'])) {
 
    $name = $_POST['name'];
    $price = $_POST['price'];
    $description = $_POST['description'];
 
	$host="localhost";      //replace with your hostname 
	$username="root";       //replace with your username 
	$password="";           //replace with your password 
	$db_name="android";     //replace with your database 

	//open connection to mysql db
	$connection = new mysqli($host,$username,$password,$db_name);
	
	//check connection 
	if(mysqli_connect_errno()) {
		$response["success"] = 0;
		$response["message"] = "Error Connection failed " .mysqli_connect_error();
		// echoing JSON response
		echo json_encode($response);
		exit();		
	}
	if(!$connection->set_charset("utf8")) {  
		$response["success"] = 0;
		$response["message"] = "Error loading character set utf8:". $connection->error;
 
		// echoing JSON response
		echo json_encode($response);
		//close the db connection
		$connection->close();
		exit(); 
	}
 
    // mysql inserting a new row
	$sql = "INSERT INTO products(name, price, description) VALUES('$name', '$price', '$description')";
	$result = $connection->query($sql);
	
 
    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Product successfully created.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
	//close the db connection
	$connection->close();
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}

?>